@tool
extends Node2D

## Click the Tog to start the code
@export var tog:bool:
	set(b):
		test()

func _prim_changed():
	print("!! _prim_changed <-- This does not run automatically.")

func _noise_changed():
	print("!! _noise_changed <-- this one works!")

func _cust_sig_func():
	print("sigfunc1")

func test():
	print()
	print()

	## Custom Resource signal is ok.
	var R:Resource = Resource.new()
	R.changed.connect(_cust_sig_func)
	R.changed.emit() # works.

	## This "built-in" seems to emit changed signal ok...
	var noise:FastNoiseLite = FastNoiseLite.new()
	noise.changed.connect(_noise_changed) # changed? YES!
	noise.domain_warp_enabled = true

	## None of these seem to emit the changed signal...
	## Docs say: Emitted when the resource changes, usually when one of its properties is modified.
	var builtinBox : BoxMesh
	builtinBox = BoxMesh.new()
	builtinBox.changed.connect(_prim_changed)
	builtinBox.set("size", Vector3(10,10,10) ) # changed? Nope.
	builtinBox.subdivide_depth = 2 # changed? Nope.

	var builtinSphere : SphereMesh
	builtinSphere = SphereMesh.new()
	builtinSphere.changed.connect(_prim_changed)
	builtinSphere.height = 2.0 # changed? Nope

	var builtinPrim : PrimitiveMesh
	builtinPrim = PrimitiveMesh.new()
	builtinPrim.changed.connect(_prim_changed)
	builtinPrim.flip_faces = true # change? Nope

